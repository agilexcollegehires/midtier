/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 * User
 * Object representation of the Users Table
 * @author vonwaldm
 */
public class User {
    
    private String email, username, password;
    private int UID;
    
    public User(int UID, String username, String email, String password){
        this.UID = UID;
        this.username = username;
        this.email = email;
        this.password = password;
    }
    
    public int getUID()
    {
        return UID;
    }
    
    public String getUsername()
    {
        return username;
    }
    
    public String getEmail()
    {
        return email;
    }
    
    public String getPassword()
    {
        return password;
    }
}
