/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * UserRequest
 * Processes requests to get data from the users table
 * @author vonwaldm
 */
public class UserRequest {
    
    public ArrayList<Object> getUserTable() {
        String query = "SELECT * FROM Users;";
        ArrayList<Object> result = new ArrayList<Object>();
        
        try {
            DBQueryHandler dbQueryHand = new DBQueryHandler();
            ResultSet rs = dbQueryHand.doQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            
            int numCols = rsmd.getColumnCount();
            result.add(new Integer(numCols));
            
            while(rs.next()) {
                int i = 1;
                int UID = rs.getInt(i++);
                String username = rs.getString(i++);
                String email = rs.getString(i++);
                String password = rs.getString(i++);
          
            
            User user = new User(UID, username, email, password);
            result.add(user);
            }
            dbQueryHand.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return result;
    }
    
}
