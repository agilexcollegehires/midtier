/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 * DBQueryHandler
 * This class handles the DB query interface
 * @author vonwaldm
 */
import java.io.*;
import java.sql.*;

public class DBQueryHandler extends DBHandler implements Serializable {
    
    /*
    Execute a query and return a ResultSet
    */
    public ResultSet doQuery(String query) throws SQLException {
        if(!isOpen)
            open();
        ResultSet rs = stmt.executeQuery(query);
        return rs;
    }
    
}
