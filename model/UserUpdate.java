/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.sql.SQLException;

/**
 * UserUpdate
 * Handles adds and deletes to the users table
 * @author vonwaldm
 */
public class UserUpdate {
    
    public boolean addUser(int UID, String username, String email, String password)
    {
        DBCommandHandler dbComHand = new DBCommandHandler();
        String command = "INSERT INTO Users(username, email, password) VALUES(";
        command += "'" + username + "'";
        command += ",'" + email + "'";
        command += ",'" + password + "'";
        command += ")";
        
        try {
            int resultCount = dbComHand.doCommand(command);
            dbComHand.close();
            return (resultCount > 0);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    //TODO deleteUser, set/update, 
    
}
