package model;

import java.io.*;
import java.sql.*;

/**
 * DBCommandHandler
 * This class handles the DB command interface
 * @author vonwaldm
 */
public class DBCommandHandler extends DBHandler implements Serializable{
    
    /*
    * Executes a command and return a result count
    */
    public int doCommand(String command) throws SQLException{
        if(!isOpen)
            open();
        int resultCount = stmt.executeUpdate(command);
        return resultCount;
    }
    
}
